[
  {
    "name": "${name}",
    "image": "${image}",
    "essential": ${essential},
    "portMappings": [
      {
        "containerPort": ${container_port},
        "hostPort": ${host_port}
      }
    ],
    "memory": ${memory},
    "cpu": ${cpu}
  }
]