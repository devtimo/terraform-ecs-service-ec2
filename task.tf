data "aws_ecs_task_definition" "this" {
  task_definition = "${aws_ecs_task_definition.this.family}"
  depends_on = [aws_ecs_task_definition.this]
}

resource "aws_ecs_task_definition" "this" {
    family                = var.name
    container_definitions = data.template_file.task_definition.rendered
}

data "template_file" "task_definition" {
  template = file("${path.module}/task.tpl")

  vars = {
      name = var.name
      image = var.image
      essential = var.essential
      container_port = var.container_port
      host_port = var.host_port
      memory = var.memory
      cpu = var.cpu
  }
}
