variable "name" {
  description = "Nome do serviço"
}

variable "vpc_id" {
  description = "VPC ID"
}

variable "iam_role" {
  description = "Role do serviço"
}

variable "cluster" {
  description = "Nome do cluster"
}

variable "priority" {
  description = "Prioridade no load balancer. Deve ser único dentro do LB."
}

variable "alb_listener_arn" {
  description = "ARN do listener do ALB"
}

variable "host_prefix" {
  description = "Prefixo do host. Exemplo: backend (-> backend.domain.com)"
}

variable "image" {
  description = "URL da imagem"
}

variable "essential" {
  default = true
}

variable "container_port" {
  default = 80
}

variable "host_port" {
  default = 0
}

variable "memory" {
  description = "Quantidade de memória RAM (em MB)"
}

variable "cpu" {
  description = "Quantidade de porcentagem de CPU (0-100)"
}