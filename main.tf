
resource "aws_lb_target_group" "this" {
  name        = "${var.name}-tg"
  port        = 80
  protocol    = "HTTP"
  target_type = "instance"
  vpc_id      = var.vpc_id
}

resource "aws_ecs_service" "this" {
  	name            = var.name
  	iam_role        = var.iam_role
  	cluster         = var.cluster
  	task_definition = "${aws_ecs_task_definition.this.family}:${max("${aws_ecs_task_definition.this.revision}", "${data.aws_ecs_task_definition.this.revision}")}"
  	desired_count   = 1

  	load_balancer {
    	target_group_arn  = aws_lb_target_group.this.id
    	container_port    = 80
    	container_name    = var.name
	}
}


resource "aws_lb_listener_rule" "this" {
  listener_arn = var.alb_listener_arn
  priority     = var.priority

  action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.this.id
  }

  condition {
    host_header {
      values = ["${var.host_prefix}.*"]
    }
  }
}